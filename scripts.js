const apiID = "b78cbf62a00b4579a0b31123230802";
const cityInput = document.querySelector(".city-input");
const cityName = document.querySelector(".city-name");
const weatherIcon = document.querySelector(".weather-icon");
const temp = document.querySelector(".temp");
const weatherCon = document.querySelector(".weather-condition")
const humidity = document.querySelector(".humi")
const wind = document.querySelector(".wind")
const time = document.querySelector(".time")
const max = document.querySelector(".max")
const min = document.querySelector(".min")
const cities = document.querySelector(".city")



cityInput.addEventListener("change", (e) => {
    fetch(`https://api.weatherapi.com/v1/forecast.json?key=${apiID}&q=${e.target.value}&days=10&aqi=no`)
        .then(async res => {
            const data = await res.json();
            cityName.innerHTML = data.location.name || "--";
            weatherIcon.setAttribute('src', data.current.condition.icon);
            temp.innerHTML = Math.round(data.current.temp_c)+"&#176" || "--";
            max.innerHTML = Math.round(data.forecast.forecastday[0].day.maxtemp_c) || "--";
            min.innerHTML = Math.round(data.forecast.forecastday[0].day.mintemp_c) || "--";
            humidity.innerHTML = data.current.humidity + " %";
            wind.innerHTML = data.current.wind_kph + " km/h";
            time.innerHTML = data.location.region;
        });
});

const forecastItems = document.querySelectorAll('.table li');
for (let i = 0; i < data.forecast.forecastday.length; i++) {
    forecastItems[i].querySelector('.days').textContent = data.forecast.forecastday[i].date;
    forecastItems[i].querySelector('img').setAttribute('src', data.forecast.forecastday[i].day.condition.icon);
    forecastItems[i].querySelector('.min').textContent = Math.round(data.forecast.forecastday[i].day.mintemp_c) + "°";
    forecastItems[i].querySelector('.max').textContent = Math.round(data.forecast.forecastday[i].day.maxtemp_c) + "°";
}